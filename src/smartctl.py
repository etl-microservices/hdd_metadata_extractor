
class Smartctl:

    def get_temperature(device):
        try:
            command = f"smartctl -a {device} | awk '/Temperature_Celsius/ {{print $10}}'"
            result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, text=True)
            temperature = result.stdout.strip()
            return temperature
        except subprocess.CalledProcessError as e:
            print(f"Error: {e}")
            return None